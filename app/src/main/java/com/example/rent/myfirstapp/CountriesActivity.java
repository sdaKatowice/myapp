package com.example.rent.myfirstapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountriesActivity extends AppCompatActivity {

    @BindView(R.id.listView)
    ListView listView;
    ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries);
        ButterKnife.bind(this);

        List<String> countries = Arrays.asList("Polska","Niemcy","Czechy","Słowacja","Belgia",
                "Holandia", "Francja","Hiszpania","Portugalia","Anglia",
                "Mozambik","Gwinea Równikowa", "RPA", "Botswana","Tanzania",
                "Kongo","Egipt","Sudan","Tunezja","Maroko");

        adapter = new ArrayAdapter<>(this,R.layout.row,countries);
        listView.setAdapter(adapter);

    }
}
