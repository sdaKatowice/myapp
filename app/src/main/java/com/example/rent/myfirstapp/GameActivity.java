package com.example.rent.myfirstapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GameActivity extends AppCompatActivity {

    private static final String TAG = "AdTag";
    private static final String SAVED_USER_RESULT = "UserResult";
    private static final String SAVED_COMP_RESULT = "CompResult";


    @BindView(R.id.imagePaper)
    ImageButton imagePaper;
    @BindView(R.id.imageScissors)
    ImageButton imageScissors;
    @BindView(R.id.imageStone)
    ImageButton imageStone;
    @BindView(R.id.imageUserChoice)
    ImageView imageUserChoice;
    @BindView(R.id.imageCompChoice)
    ImageView imageCompChoice;
    @BindView(R.id.gameTextViewUserResult)
    TextView textViewUserResult;
    @BindView(R.id.gameTextViewCompResult)
    TextView textViewCompResult;

    private static int compImageIndex;
    private static int userResult;
    private static int compResult;

    List<Integer> images = Arrays.asList(0, 1, 2);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        ButterKnife.bind(this);

        textViewCompResult.addTextChangedListener(compResultTextWatcher);
        textViewUserResult.addTextChangedListener(userResultTextWatcher);

        imagePaper.setImageResource(R.drawable.paper);
        imageScissors.setImageResource(R.drawable.scissors);
        imageStone.setImageResource(R.drawable.stone);
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: called");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState: " + String.valueOf(userResult));
        outState.putInt(SAVED_USER_RESULT,userResult);
        outState.putInt(SAVED_COMP_RESULT,compResult);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        userResult = savedInstanceState.getInt(SAVED_USER_RESULT);
        Log.d(TAG, "onRestoreInstanceState: " + String.valueOf(userResult));
        textViewUserResult.setText(String.valueOf(userResult));

        compResult = savedInstanceState.getInt(SAVED_COMP_RESULT);
        textViewCompResult.setText(String.valueOf(compResult));
    }

    @OnClick(R.id.imagePaper)
    public void buttonPaperClicked() {
        imageUserChoice.setImageResource(R.drawable.paper);
        setCompImage();
        playRound(1,2);
    }

    @OnClick(R.id.imageScissors)
    public void buttonScissorsClicked() {
        imageUserChoice.setImageResource(R.drawable.scissors);
        setCompImage();
        playRound(2,0);
    }

    @OnClick(R.id.imageStone)
    public void buttonStoneClicked() {
        imageUserChoice.setImageResource(R.drawable.stone);
        setCompImage();
        playRound(0,1);
    }

    private void playRound(int compPositiveValue, int compNegativeValue) {
        if (compImageIndex == compPositiveValue) {
            textViewCompResult.setText(String.valueOf(++compResult));
        } else if (compImageIndex == compNegativeValue) {
            textViewUserResult.setText(String.valueOf(++userResult));
        }
    }

    private void setCompImage() {
        Random random = new Random();
        int randomImage = images.get(random.nextInt(3));
        switch (randomImage) {
            case 0:
                imageCompChoice.setImageResource(R.drawable.paper);
                compImageIndex = 0;
                break;
            case 1:
                imageCompChoice.setImageResource(R.drawable.scissors);
                compImageIndex = 1;
                break;
            case 2:
                imageCompChoice.setImageResource(R.drawable.stone);
                compImageIndex = 2;
                break;
        }
    }

    private final TextWatcher compResultTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().equals("5")) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(GameActivity.this);
                alertDialogBuilder.setTitle("Niestety przegrałeś!")
                        .setMessage("Czy chcesz zagrać kolejny raz?")
                        .setPositiveButton("TAK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                restartScores();
                            }
                        })
                        .setNegativeButton("NIE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                restartScores();
                                finish();
                            }
                        }).setCancelable(false).show();
            }
        }
    };

    private void restartScores() {
        userResult = 0;
        compResult = 0;
        textViewUserResult.setText(String.valueOf(userResult));
        textViewCompResult.setText(String.valueOf(compResult));
        imageUserChoice.setImageResource(R.drawable.transparent);
        imageCompChoice.setImageResource(R.drawable.transparent);
    }

    private final TextWatcher userResultTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if (s.toString().equals("5")) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(GameActivity.this);
                alertDialogBuilder.setTitle("Gratulacje! Wygrałeś rundę!")
                        .setMessage("Czy chcesz zagrać kolejny raz?")
                        .setPositiveButton("TAK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                restartScores();
                            }
                        })
                        .setNegativeButton("NIE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).setCancelable(false).show();
            }

        }
    };
}