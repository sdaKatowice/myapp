package com.example.rent.myfirstapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.example.rent.myfirstapp.databinding.ActivityUserDataBinding;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserDataActivity extends AppCompatActivity {

    @BindView(R.id.birthDate)
    EditText birthDate;
    @BindView(R.id.btnAge)
    Button btnUserAge;
    @BindView(R.id.userAge)
    TextView userAge;

    private static final String TAG = "UserDataActivity";

    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityUserDataBinding userDataBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_user_data);

        ButterKnife.bind(this);

        User newUser = (User) getIntent().getSerializableExtra("user");
        userDataBinding.setUser(newUser);

//        if (newUser.isAdditionalMaterialsByMail) {
//            AlertDialog.Builder message = new AlertDialog.Builder(UserDataActivity.this);
//            message.setTitle("Dodatkowe materiały")
//                    .setMessage("Dodatkowe materiały dostępne są na dropboxie")
//                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//
//                        }
//                    }).setCancelable(false).show();
//        }
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    @OnClick(R.id.birthDate)
    public void onClickBirthDate() {
        new DatePickerDialog(UserDataActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        birthDate.setText(sdf.format(myCalendar.getTime()));
    }

    @OnClick(R.id.btnAge)
    public void onClickBtnAge() {
        int userBirthYear = myCalendar.get(Calendar.YEAR);
        int intUserAge = 2017 - userBirthYear;
        userAge.setText(getResources().getString(R.string.user_age).concat(String.valueOf(intUserAge)));
//        userAge.setText(String.format("Wiek użytkownika: %s", String.valueOf(intUserAge)));
    }
}
