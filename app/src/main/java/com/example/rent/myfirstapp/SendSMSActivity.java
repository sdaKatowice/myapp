package com.example.rent.myfirstapp;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SendSMSActivity extends AppCompatActivity {

    EditText phoneNumber;
    EditText textSMS;

//    String phoneNo;
//    String message;

    Button btnPermission;
    Button btnSendSMS;
    Button btnNumberChange;

    final String TAG = "AdApp";
    private static final int MY_PERMISSIONS_REQUEST_WRITE_SMS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);

        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        textSMS = (EditText) findViewById(R.id.textSMS);


        btnPermission = (Button) findViewById(R.id.buttonPerm);
        btnSendSMS = (Button) findViewById(R.id.buttonSendSMS);
        btnNumberChange = (Button) findViewById(R.id.numberChange);

        btnSendSMS.setOnClickListener(onSMSClick);
        btnPermission.setOnClickListener(onPermissionClick);
        btnNumberChange.setOnClickListener(onNumberChangeClick);


    }

    public View.OnClickListener onNumberChangeClick = new View.OnClickListener() {
        public void onClick(View v) {
            String stringValue = phoneNumber.getText().toString();
            int resultP1 = Integer.valueOf(stringValue) / 8;
            int resultP2 = resultP1 + Integer.valueOf(stringValue.substring(0, 3));
            int resultP3 = resultP2 - Integer.valueOf(stringValue.substring(3, 5));
            textSMS.setText(phoneNumber.getText().toString() + " build:" +
                    "\n" + resultP1 +
                    "\n" + resultP2 +
                    "\n" + resultP3);
            Log.d(TAG, String.valueOf(resultP3));
        }
    };

    public View.OnClickListener onSMSClick = new View.OnClickListener() {
        public void onClick(View v) {
            sendSMS(phoneNumber.getText().toString(), textSMS.getText().toString());
        }


    };

    private void sendSMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Log.d(TAG, "SMS Wysłany");
            phoneNumber.setText("");
            textSMS.getText().clear();
        } catch (Exception e) {
            Log.d(TAG, "SMS Nie wysłany");
            e.printStackTrace();
        }

    }

    public View.OnClickListener onPermissionClick = new View.OnClickListener() {
        public void onClick(View v) {

            // sprawdzamy czy jest przyznay dostep
            if (android.support.v4.app.ActivityCompat.checkSelfPermission(SendSMSActivity.this, Manifest.permission.SEND_SMS)
                    != PackageManager.PERMISSION_GRANTED) {

                // za pomoca tej funkcji sprawdzamy czy uzytkownik po raz pierwszy juz blokowal dostep do sms
                if (ActivityCompat.shouldShowRequestPermissionRationale(SendSMSActivity.this,
                        Manifest.permission.SEND_SMS)) {
                    // jesli dostep blokowal pokazujemy po co nam to potrzebne
                    showExplanation("Potrzebujemy pozwolenia", "Chcemy wysłać SMS który napisałeś, więc potrzebujemy pozwolenia",
                            Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_WRITE_SMS);
                } else {
                    // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
                    requestPermissions(Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_WRITE_SMS);
                }
            } else {
                // wlaczamy przycisk sms jesli dostep przyznay ( mozna to sprawdzac z automatu w oncreate aby za kazdym razem nie sprawdzac tutaj)
                btnSendSMS.setEnabled(true);
            }
        }
    };

    private void requestPermissions(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this, new String[]{permissionName}, permissionRequestCode);
    }

    private void showExplanation(String title, String message, final String permission, final int permissionRequestCode) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestPermissions(permission, permissionRequestCode);
            }
        }).show();
    }
}
