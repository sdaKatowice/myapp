package com.example.rent.myfirstapp;

import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResActivity extends AppCompatActivity {

    @BindView(R.id.btnPlay) Button btnPlay;
    @BindView(R.id.btnZoom) Button btnZoom;
    @BindView(R.id.btnBlink) Button btnBlink;
    @BindView(R.id.btnRotate) Button btnRotate;
    @BindView(R.id.btnMove) Button btnMove;
    @BindView(R.id.androidImage) ImageView androidImage;

    MediaPlayer mPlayer;
    AnimationDrawable frameAnimation;
    AnimationDrawable frameAnimation2;

    static boolean isMusicPlaying = false;
    static boolean isImageZoomed = false;
    static boolean isImageBlinking = false;
    static int imageRotation;
    private static String TAG = "AdTag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_res);

        ButterKnife.bind(this);

    }

    @OnClick(R.id.btnMove)
    public void moveImage() {
        Log.d(TAG,"MOVE");
        androidImage.setImageResource(R.drawable.image_move);
        frameAnimation2 = (AnimationDrawable) androidImage.getDrawable();
        frameAnimation2.start();
    }

    @OnClick(R.id.btnZoom)
    public void zoomImage() {
        if (!isImageZoomed) {
            androidImage.setScaleX(2);
            androidImage.setScaleY(2);
            isImageZoomed = true;
        } else {
            androidImage.setScaleX(1);
            androidImage.setScaleY(1);
            isImageZoomed = false;
        }
    }

    @OnClick(R.id.btnRotate)
    public void rotateImage() {
        imageRotation+=90;
        androidImage.setRotation(imageRotation);

    }

    @OnClick(R.id.btnBlink)
    public void blinkImage() {
        if (!isImageBlinking) {
            androidImage.setImageResource(R.drawable.blinking_image);
            frameAnimation = (AnimationDrawable) androidImage.getDrawable();
            frameAnimation.start();
            isImageBlinking = true;
        } else {
            frameAnimation.stop();
            androidImage.setImageResource(R.mipmap.ic_launcher);
            isImageBlinking = false;
        }

    }

    @OnClick(R.id.btnPlay)
    public void playMusic() {
        Log.d(TAG,"boolean value:" + String.valueOf(isMusicPlaying));
        if (!isMusicPlaying) {
            mPlayer = MediaPlayer.create(ResActivity.this, R.raw.supermario_bpi7cych);
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayer.start();
            isMusicPlaying = true;
        } else {
            mPlayer.stop();
            isMusicPlaying = false;
        }

    }


}
