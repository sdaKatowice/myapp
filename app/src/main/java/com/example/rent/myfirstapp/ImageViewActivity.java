package com.example.rent.myfirstapp;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.secretphoto.PhotoSecretReturner;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ViewScaleType;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageViewActivity extends AppCompatActivity {

    private static String TAG = "AdTag";

    @BindView(R.id.imageView) ImageView imageView;
    @BindView(R.id.button) Button button;



    @OnClick(R.id.button)
    public void loadImage() {

        String currentImage = PhotoSecretReturner.createRandomPhoto();
        Log.d(TAG,currentImage);

        ImageLoader.getInstance().displayImage(currentImage, imageView);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
        ButterKnife.bind(this);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);
        String image ="http://halfmiletiming.com/wp-content/uploads/2016/03/imglogo.png";
        ImageLoader.getInstance().displayImage(image, imageView);
    }



}
