package com.example.rent.myfirstapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    final String TAG = "AdApp";
    String wpisanyTekst;
    EditText wpisanyAdresUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "MainActivity: onCreate()");

        wpisanyAdresUrl = (EditText) findViewById(R.id.editText);
        Button btnDalejNazwa = (Button) findViewById(R.id.btnDalej);



        btnDalejNazwa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wpisanyTekst = wpisanyAdresUrl.getText().toString();
                Log.d(TAG, wpisanyTekst);

                Intent main2 = new Intent(MainActivity.this, Main2Activity.class);
                main2.putExtra("text",wpisanyTekst);
                startActivity(main2);
            }
        });

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "MainActivity: onRestart()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "MainActivity: onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "MainActivity: onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "MainActivity: onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "MainActivity: onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "MainActivity: onDestroy()");
    }
}
