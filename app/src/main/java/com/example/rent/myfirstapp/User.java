package com.example.rent.myfirstapp;


import java.io.Serializable;

public class User implements Serializable{
    private String name;
    private String postalCode;
    private boolean interestedInEveningCourse;
    private boolean additionalMaterialsByMail;
    private String phoneNumber;
    private String country;

    public User(String name, String postalCode,
                boolean interestedInEveningCourse, boolean additionalMaterialsByMail,
                String phoneNumber, String country) {
        this.name = name;
        this.postalCode = postalCode;
        this.interestedInEveningCourse = interestedInEveningCourse;
        this.additionalMaterialsByMail = additionalMaterialsByMail;
        this.phoneNumber = phoneNumber;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public boolean isInterestedInEveningCourse() {
        return interestedInEveningCourse;
    }

    public boolean isAdditionalMaterialsByMail() {
        return additionalMaterialsByMail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public String interestedInEveningCourse() {
        if (interestedInEveningCourse) {
            return "Zainteresowany kursem wieczorowym";
        } else {
            return "Niezainteresowany kursem wieczorowym";
        }
    }

    public String additionalMaterialsByMail() {
        if (additionalMaterialsByMail) {
            return "Chce otrzymywać dodatkowe materiał pocztą";
        } else
            return "Nie chce otrzymywać dodatkowych materiałów pocztą";
    }
}
