package com.example.rent.myfirstapp;

import java.text.DecimalFormat;

import android.animation.TimeInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CalculatorActivity extends AppCompatActivity {

    @BindView(R.id.imageButton)
    ImageButton imageButton;
    @BindView(R.id.editTextHorizontal)
    EditText horizontal;
    @BindView(R.id.editTextVertical)
    EditText vertical;
    @BindView(R.id.editTextDiagonal)
    EditText diagonal;
    @BindView(R.id.textView2)
    TextView textView;
    DecimalFormat df;

    ViewPropertyAnimator animator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        ButterKnife.bind(this);


    }


    @OnClick(R.id.imageButton)
    public void calculate() {
        if (!(horizontal.getText().toString().equals("") ||
                vertical.getText().toString().equals("") ||
                diagonal.getText().toString().equals(""))) {
            double horizontalValue = Double.valueOf(horizontal.getText().toString());
            double verticalValue = Double.valueOf(vertical.getText().toString());
            double diagonalValue = Double.valueOf(diagonal.getText().toString());

            df = new DecimalFormat();
            df.setMaximumFractionDigits(2);

            imageButton.setRotation(0);

            ViewPropertyAnimator animator = imageButton.animate();
            animator.setDuration(1000).rotation(720).start();
//            animator.setDuration(500).scaleXBy(1.1f).scaleYBy(1.1f).start();
//            animator.setStartDelay(1000).scaleXBy(0.5f).scaleYBy(0.5f).start();


            double result = Math.sqrt((Math.pow(horizontalValue, 2) + Math.pow(verticalValue, 2))) / diagonalValue;
            textView.setText("RESULT: " + String.valueOf(df.format(result)) + " PPI");
        }

    }
}
