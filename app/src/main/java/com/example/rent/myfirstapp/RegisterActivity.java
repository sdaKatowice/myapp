package com.example.rent.myfirstapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.os.PersistableBundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.Switch;
import android.widget.ToggleButton;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {

    private final String TAG = "AdTag";

    @BindView(R.id.registerUsernameWrapper)
    TextInputLayout registerUsernameWrapper;
    @BindView(R.id.registerPostalCodeWrapper)
    TextInputLayout registerPostalCodeWrapper;
    @BindView(R.id.registerPhoneNumberWrapper)
    TextInputLayout registerPhoneNumberWrapper;
    @BindView(R.id.autoCompleteTextView)
    AutoCompleteTextView autoCompleteTextView;
    @BindView(R.id.registerChckbxAccept)
    CheckBox registerCheckbox;
    @BindView(R.id.registerBtn)
    Button registerBtn;
    @BindView(R.id.registerSwitch)
    Switch registerSwitch;
    @BindView(R.id.registerToggleBtn)
    ToggleButton registerToggleBtn;

    private static final String POSTAL_CODE_PATTERN = "^\\d{2}-\\d{3}$";
    private static final String PHONE_NUMBER_PATTERN = "^\\d{3} \\d{3} \\d{3}$";

    private Pattern postalCodePattern = Pattern.compile(POSTAL_CODE_PATTERN);
    private Pattern phoneNumberPattern = Pattern.compile(PHONE_NUMBER_PATTERN);
    private Matcher matcher;


    private boolean validatePostalCode(String postalCode) {
        matcher = postalCodePattern.matcher(postalCode);
        return matcher.matches();
    }

    private boolean validatePhoneNumber(String phoneNumber) {
        matcher = phoneNumberPattern.matcher(phoneNumber);
        return matcher.matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        int layoutItemId = android.R.layout.simple_dropdown_item_1line;
        final List<String> countriesList = Arrays.asList(getResources().getStringArray(R.array.countries));
//        final String[] countries = (String[]) countriesList.toArray();
        final String[] countries = new String[]{"","Poland","Portugal","Persia"};

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, layoutItemId, countriesList);

        ButterKnife.bind(this);
        autoCompleteTextView.setAdapter(adapter);

        registerPostalCodeWrapper.getEditText().addTextChangedListener(postalCodeTextWatcher);
        registerPhoneNumberWrapper.getEditText().addTextChangedListener(phoneNumberTextWatcher);

        autoCompleteTextView.setValidator(new AutoCompleteTextView.Validator() {
            @Override
            public boolean isValid(CharSequence text) {
                Log.d("Test", "Checking if valid: "+ text);
                Arrays.sort(countries);
                if (Arrays.binarySearch(countries, text.toString()) > 0) {
                    return true;
                }

                return false;
            }

            @Override
            public CharSequence fixText(CharSequence invalidText) {
                Log.d("Test", "Returning fixed text");
                return "hhh";
            }
        });

        autoCompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.d("Test", "Focus changed");
                if (v.getId() == R.id.autoCompleteTextView && !hasFocus) {
                    ((AutoCompleteTextView)v).performValidation();
                }
            }
        });
    }

    @OnClick(R.id.registerBtn)
    public void onClickRegisterBtn() {
        String postalCode = registerPostalCodeWrapper.getEditText().getText().toString();
        String phoneNumber = registerPhoneNumberWrapper.getEditText().getText().toString();

        if (!validatePostalCode(postalCode)) {
            registerPostalCodeWrapper.setError("Invalid postal code type");
        } else {
            registerPostalCodeWrapper.setErrorEnabled(false);
        }

        if (!validatePhoneNumber(phoneNumber)) {
            registerPhoneNumberWrapper.setError("Invalid phone number type");
        } else {
            registerPhoneNumberWrapper.setErrorEnabled(false);
        }

        if (registerUsernameWrapper.getEditText().getText().toString().isEmpty()) {
            registerUsernameWrapper.setError("Pole nie może być puste");
            Log.d(TAG,"pac pac plum");
        } else {
            registerUsernameWrapper.setErrorEnabled(false);
        }

        if (!registerCheckbox.isChecked()) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterActivity.this);
            alertDialogBuilder.setTitle("Wymagana akceptacja regulaminu")
                    .setMessage("Aby zakończyć rejestrację musisz zaakceptować regulamin")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
        }
        Intent intent = new Intent(RegisterActivity.this, UserDataActivity.class);
        User user = new User(registerUsernameWrapper.getEditText().getText().toString(),
                postalCode,
                registerSwitch.isChecked(),
                registerToggleBtn.isChecked(),
                phoneNumber,
                autoCompleteTextView.getText().toString());
        intent.putExtra("user",user);

        startActivity(intent);

    }

    private final TextWatcher postalCodeTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            registerPostalCodeWrapper.setErrorEnabled(false);
            if (s.length() == 3 && s.toString().charAt(s.length() - 1) != '-') {
                String postalCodeValue = registerPostalCodeWrapper.getEditText().getText().toString();
                String lastValue = String.valueOf(postalCodeValue.charAt(postalCodeValue.length() - 1));
                String firstPart = postalCodeValue.substring(0, 2);
                postalCodeValue = firstPart + "-" + lastValue;
                registerPostalCodeWrapper.getEditText().setText(postalCodeValue);
                registerPostalCodeWrapper.getEditText().setSelection(postalCodeValue.length());
            }
        }
    };

    private final TextWatcher phoneNumberTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            registerPhoneNumberWrapper.setErrorEnabled(false);
            if (s.length() == 4 && s.toString().charAt(s.length() - 1) != ' ') {
                String phoneNumber = registerPhoneNumberWrapper.getEditText().getText().toString();
                String lastValue = String.valueOf(phoneNumber.charAt(phoneNumber.length() - 1));
                String firstPart = phoneNumber.substring(0, 3);
                phoneNumber = firstPart + " " + lastValue;
                registerPhoneNumberWrapper.getEditText().setText(phoneNumber);
                registerPhoneNumberWrapper.getEditText().setSelection(phoneNumber.length());
            } else if (s.length() == 8 && s.toString().charAt(s.length() - 1) != ' ') {
                String phoneNumber = registerPhoneNumberWrapper.getEditText().getText().toString();
                String lastValue = String.valueOf(phoneNumber.charAt(phoneNumber.length() - 1));
                String firstPart = phoneNumber.substring(0, 7);
                phoneNumber = firstPart + " " + lastValue;
                registerPhoneNumberWrapper.getEditText().setText(phoneNumber);
                registerPhoneNumberWrapper.getEditText().setSelection(phoneNumber.length());
            }
        }
    };

}
