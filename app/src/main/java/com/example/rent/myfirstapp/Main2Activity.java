package com.example.rent.myfirstapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class Main2Activity extends AppCompatActivity {

    String przypisanyAdres;
    final String TAG = "AdApp";

    @BindView(R.id.textView) TextView textView;
    @BindView(R.id.webView) WebView webView;

    @OnClick(R.id.btnOpenSite)
    public void blabla() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(correctUrl(przypisanyAdres)));
        startActivity(browserIntent);
    }
    @OnLongClick(R.id.btnOpenSite)
    public boolean dupa() {
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(correctUrl(przypisanyAdres));
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);

        final Intent main2 = getIntent();
        przypisanyAdres = main2.getStringExtra("text");

        textView.setText(correctUrl(przypisanyAdres));

    }

    private String correctUrl(String address) {
        return address.startsWith("http://") ? address.toLowerCase() : "http://" + address.toLowerCase();
    }
}
