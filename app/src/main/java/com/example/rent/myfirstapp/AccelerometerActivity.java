package com.example.rent.myfirstapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.percent.PercentRelativeLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccelerometerActivity extends AppCompatActivity implements SensorEventListener {

    @BindView(R.id.valueX) TextView valueX;
    @BindView(R.id.valueY) TextView valueY;
    @BindView(R.id.valueZ) TextView valueZ;
    @BindView(R.id.temp) TextView temp;
    @BindView(R.id.layout) PercentRelativeLayout layout;

    final String TAG = "AdApp";

    private static final int SHAKE_THRESHOLD = 800;

    private float mLastX=-1.0f, mLastY=-1.0f, mLastZ=-1.0f;
    long lastUpdate;


    SensorManager mSensorManager;
    Sensor mAccelerometer;
    Sensor mTemperature;
    DecimalFormat df;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometer);
        ButterKnife.bind(this);

        df = new DecimalFormat();
        df.setMaximumFractionDigits(1);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mTemperature = mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);

        mSensorManager.registerListener(this,
                mAccelerometer,
                SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this,mTemperature,SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSensorManager.registerListener(this,
                mAccelerometer,
                SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this,mTemperature,SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor mySensor = event.sensor;
        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            valueX.setText(String.valueOf(df.format(event.values[0])));
            valueY.setText(String.valueOf(df.format(event.values[1])));
            valueZ.setText(String.valueOf(df.format(event.values[2])));
            long curTime = System.currentTimeMillis();
            // only allow one update every 100ms.
            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;
                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];

                float speed = Math.abs(x+y+z - mLastX - mLastY - mLastZ) / diffTime * 10000;

                if (speed > SHAKE_THRESHOLD) {
                    Log.d("sensor", "shake detected w/ speed: " + speed);
                    layout.setBackgroundColor(Color.rgb((int) x/10,(int) y/10,(int) z/10));
                    Toast.makeText(this, "shake detected w/ speed: " + speed, Toast.LENGTH_SHORT).show();
                }
                mLastX = x;
                mLastY = y;
                mLastZ = z;
            }
        }
        if (mySensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE) {
//            layout.setBackgroundColor(Color.argb(0,(int) -(273.1 + event.values[0]),0,0));
            temp.setText(String.valueOf(273.1 + event.values[0]));
            Log.d(TAG, String.valueOf(event.values[0]));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
