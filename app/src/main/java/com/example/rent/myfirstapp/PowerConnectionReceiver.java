package com.example.rent.myfirstapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class PowerConnectionReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction() == Intent.ACTION_POWER_CONNECTED) {
            Toast.makeText(context, "Charger PluggedIn", Toast.LENGTH_SHORT).show();
        } else if(intent.getAction() == Intent.ACTION_POWER_DISCONNECTED){
            Toast.makeText(context, "Charger PluggedOut", Toast.LENGTH_SHORT).show();
        }
    }
}