package com.example.secretphoto;


import android.util.Log;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PhotoSecretReturner {

    private static int currentImageIndex = -1;
    private static List<String> photos = Arrays.asList(
            "https://iso.500px.com/wp-content/uploads/2016/03/stock-photo-141092249-1500x1000.jpg",
            "https://cdn.pixabay.com/photo/2017/03/17/16/27/shell-2152029_960_720.jpg",
            "https://i.gadgets360cdn.com/large/samsung_galaxy_s8_samsung_display_1485501272554.jpg?output-quality=80",
            "https://static.pexels.com/photos/235294/pexels-photo-235294.jpeg",
            "http://www.ufunk.net/wp-content/uploads/2016/08/Lucas-Levitan-photo-invasion-part-3-9.jpg",
            "https://d3i6fh83elv35t.cloudfront.net/newshour/wp-content/uploads/2016/11/Mario-Fiorucci.jpg",
            "https://lh3.googleusercontent.com/8zLSFY3LKoX6R3yhuSEOHPRXgv9Hd4W9ostByExjKuCC74Pl2o235Js7Wre9o--iwms=h900",
            "https://images.pexels.com/photos/300122/pexels-photo-300122.jpeg?h=350&auto=compress&cs=tinysrgb",
            "http://drop.ndtv.com/albums/COOKS/best-time/bananas.jpg");



    public static String createRandomPhoto() {
        Random random = new Random();
        while (true) {
            int randomImageIndex = random.nextInt(photos.size());
            if (randomImageIndex != currentImageIndex) {
                currentImageIndex = randomImageIndex;
                return photos.get(currentImageIndex);
            }
        }

    }
}
